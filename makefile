BINARY := nebula
export VERSION ?= vlatest

export GOC := go
export UPDATE := $(GOC) get -u

export LINTER := gometalinter

export BIN_DIR := $(GOPATH)/bin
export SOURCE_DIR := $(GOPATH)/src/gitlab.com/cosban
export PLUGINS_DIR := $(RELEASE_DIR)/plugins
export LINT := $(BIN_DIR)/$(LINTER)
export BUILD := GO111MODULE=on CC=gcc GOOS=linux GOARCH=amd64 $(GOC) build
export TEST := GO111MODULE=on CC=gcc $(GOC) test

export PKGS := $(shell go list ./... | grep -v /vendor)
export HASH := $(shell git rev-parse --short HEAD)

.PHONY: all
all: build

.PHONY: clean
clean:
	rm -rf $(RELEASE_DIR) $(PLUGINS_DIR)

$(LINT):
	$(UPDATE) github.com/alecthomas/gometalinter
	$(LINT) --install

.PHONY: lint
lint: $(LINT)
	$(LINT) ./... --vendor --disable-all --enable=vet --enable=vetshadow --enable=deadcode --enable=staticcheck --enable=varcheck --enable=interfacer

.PHONY: test
test:
	$(TEST) -v ./...

.PHONY: linux
linux:
	mkdir -p $(RELEASE_DIR)
	$(BUILD) -o $(RELEASE_DIR)/$(BINARY)
	
.PHONY: build
build: linux